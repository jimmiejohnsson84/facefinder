import QtQuick 2.4
import Ubuntu.Components 1.3
import Ubuntu.Components.Popups 1.3

Dialog 
{
    id: startupDlg
    
    Column
    {
        spacing: 1
        TextEdit
        {
            id: introTextPart1
            width:  units.gu(23)
            height: units.gu(30)
            textFormat: TextEdit.RichText
            text: "<b>Welcome to the Face Finder</b><p>This app scans .jpeg/.jpg files for faces from the folders you selected</p>\
<p>It uses the Dlib library for face recognition. It is recommended you use UT Tweak Tool and set Prevent app suspension, so the Face Finder can keep scanning while its in the background</p>"
            readOnly: true
            wrapMode: TextEdit.WordWrap
        }
        
        TextEdit
        {
            id: introTextPart2
            width:  units.gu(23)
            height: units.gu(5)
            textFormat: TextEdit.RichText
            text: "<b>Thanks for using the Facing Finder!</b>"
            readOnly: true
            wrapMode: TextEdit.WordWrap
        }
        
        Button 
        {
            id:copyOK
            text: i18n.tr("OK")
            onClicked: 
            {
                PopupUtils.close(startupDlg)
            }
        }
    }
}
