import QtQuick 2.4
import Ubuntu.Components 1.3
import Ubuntu.Components.Popups 1.3

Dialog 
{
    id: startupDlg
    
    Column
    {
        spacing: 4
        
        
        Text
        {
            id: aboutText
            width:  units.gu(23)
            height: units.gu(50)            
            text: "<b>Face Finder</b><p>Version 1.0 made by Jimmie Johnsson, NrNi9e on www.patreon.com (<a href=\"http://www.patreon.com/user?u=8973964\">Visit patreon site</a>)</p>\
<p>The face Finder is made using the Dlib library (<a href=\"http://www.dlib.net\">Dlib</a>)</p>\
<p>It is recommended you use UT Tweak Tool and set Prevent app suspension, so the Face Finder can keep scanning while its in the background</p>\
<p>Questions? Suggestions? Bugs? Contact: jimmiejohnsson84@gmail.com</p>\
<p>Thanks for using the Face Finder!</p>"            
            onLinkActivated: Qt.openUrlExternally(link)
            wrapMode: TextEdit.WordWrap
        }        
        Button 
        {
            id:copyOK
            text: i18n.tr("OK")
            onClicked: 
            {
                PopupUtils.close(startupDlg)
            }
        }
    }
}
