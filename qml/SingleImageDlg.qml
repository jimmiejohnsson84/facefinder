import QtQuick 2.4
import Ubuntu.Components 1.3
import Ubuntu.Components.Popups 1.3

Dialog 
{
    id: singleImageDlg

    Image
    {
        id:faceSingleImage
        visible:true
        fillMode: Image.PreserveAspectFit
        source: faceListModelObj.getSelectedImgSource()
        width: units.gu(55)
        height: units.gu(50)
    }

    Row
    {
        Button 
        {
            id:copyOK
            text: i18n.tr("OK")
            onClicked: 
            {
                PopupUtils.close(singleImageDlg)
            }
        }
    }
}
