import QtQuick 2.4
import Ubuntu.Components 1.3
import Ubuntu.Components.Popups 1.3

Dialog 
{
    id: clearSourcesDlg
    
    Column
    {
        Row
        {
            spacing: 2
            Button 
            {
                id:warningOk
                text: i18n.tr("OK")
                onClicked: 
                {
                    PopupUtils.close(clearSourcesDlg)
                    performClearSources()
                }
            }
            Button 
            {
                id:warningCancel
                text: i18n.tr("Cancel")
                onClicked: 
                {
                    PopupUtils.close(clearSourcesDlg)
                }
            }
        }
    }
}
