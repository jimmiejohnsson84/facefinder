import QtQuick 2.4
import QtQuick.Layouts 1.1
import Ubuntu.Components 1.3
import FaceFinder 1.0
import Ubuntu.Components.Popups 1.3

MainView
{
    id: mainView
    objectName: 'mainView'
    applicationName: 'facefinder.jimmiejohnsson'
    automaticOrientation: true

    width: units.gu(45)
    height: units.gu(75)
   
    FaceFinderHelper
    {
        id:faceFinderObj
    }
    
    DirectoryBrowser
    {
        id:directoryBrowserObj
    }
    
    SearchSourcesListModel
    {
        id:searchSourcesListModelObj
    }
    
    FaceListModel
    {
        id:faceListModelObj
    }
    
    function showChooseSources()
    {
        introTextPart1.visible = true
        indexPath.visible = true
        inputIndexPathRect.visible = true
        buttonAddFolder.visible = true
        folderListRectangle.visible = true
        folderList.visible = true    
    }    
    function hideChooseSources()
    {
        introTextPart1.visible = false
        indexPath.visible = false
        inputIndexPathRect.visible = false
        buttonAddFolder.visible = false
        folderListRectangle.visible = false
        folderList.visible = false
    }

    function showShowFaces()
    {
        facesGridView.visible = true
    }   
    function hideShowFaces()
    {
        facesGridView.visible = false
    }

    function showSearch()
    {
        searchViewText.visible = true
        searchSourcesListView.visible = true
        buttonPerformSearch.visible = true
        buttonClearSources.visible = true
        searchSourcesListViewRectangle.visible = true

    }   
    function hideSearch()
    {
        searchViewText.visible = false
        searchSourcesListView.visible = false
        buttonPerformSearch.visible = false
        buttonClearSources.visible = false
        searchSourcesListViewRectangle.visible = false

    }

    
    function populateSearchSourceListModelObj()
    {
        searchSourcesListModelObj.populateFromFoldersToIndex()     
    }
    
    function updateStatusBar()
    {
        var statusText = "<b>Images scanned: " + faceFinderObj.nrFilesScanned() + " / " + faceFinderObj.totalNrFiles() + "</b>"
        statusBar.text = statusText
        
    }
    
    function performClearSources()
    {
        faceFinderObj.clearFoldersToIndex()
        PopupUtils.open(Qt.resolvedUrl("InfoDlg.qml"), mainView,
        {
            title: "Cleared",
            text: "Please restart the app"
        })        
    }
    
    function refreshImagesInFaceViewGridAndPollProcessFiles()
    {
        // Called periodcally to update faces in the grid view
        if(facesGridView.visible)
        {
            faceListModelObj.updateModel()
        }
        
        faceFinderObj.pollProcessFiles()
    }
        
    
        
    Page
    {
        anchors.fill: parent

        header: PageHeader
        {
            id: header
            title: i18n.tr('Face Finder')
        }
        
        Item 
        {
            id: buttonArea
            anchors.left: parent.left
            anchors.top: header.bottom
            anchors.leftMargin : 4
            width: units.gu(45)
            height: units.gu(5)  
            Row
            {
                spacing: units.gu(1)
                Button
                {
                    id:buttonSources
                    width: units.gu(15)
                    height: units.gu(4)
                    text: "Choose Folders"
                    onClicked:
                    {
                        hideShowFaces()
                        hideSearch()
                        
                        showChooseSources()
                    }
                }                               

                Button
                {
                    id:buttonSearch
                    width: units.gu(8)
                    height: units.gu(4)
                    text: "Search"
                    onClicked:
                    {
                        hideShowFaces()                                                
                        hideChooseSources()
                                                                    
                        populateSearchSourceListModelObj()                                                
                        showSearch()
                    }
                }                

                Button
                {
                    id:buttonShowFaces
                    width: units.gu(13)
                    height: units.gu(4)
                    text: "Show faces"
                    onClicked:
                    {
                        faceListModelObj.updateModel()
                        
                        hideSearch()                        
                        hideChooseSources()
                        
                        showShowFaces()                        
                    }
                }
                
                Button
                {
                    id:buttonAbout
                    width: units.gu(7)
                    height: units.gu(4)
                    text: "About"
                    onClicked:
                    {
                        PopupUtils.open(Qt.resolvedUrl("AboutDlg.qml"), mainView,
                        {
                            title: "About Face Finder"
                        })                    
                    }
                }
                
            }
        }
        

        /////////////////// Search View ///////////////////
        TextEdit
        {
            id: searchViewText
            anchors.left: parent.left
            anchors.top: buttonArea.bottom            
            width:  units.gu(43)
            height: units.gu(10)
            textFormat: TextEdit.RichText
            text: "<b>Search</b><p>These are the folders that will be searched. Press the Do Search to start scanning now.</p>"
            readOnly: true
            visible: false
            wrapMode: TextEdit.WordWrap
        }      
        Button
        {
            id:buttonPerformSearch
            anchors.left: parent.left
            anchors.top: searchViewText.bottom
            visible: false
        
            width: units.gu(12)
            height: units.gu(4)
            text: "Do Search"
            onClicked:
            {
                faceFinderObj.processFiles()
                
                PopupUtils.open(Qt.resolvedUrl("DoSearchDlg.qml"), mainView,
                {
                    title: "Searching",
                    text: "Search is now running in background. Click Show Faces to view result as search is on going. An average size jpeg can take 1-2 minutes to scan, so please be patient"
                })                
            }
        }
        Button
        {
            id:buttonClearSources
            anchors.left: buttonPerformSearch.right
            anchors.top: searchViewText.bottom
            visible: false
        
            width: units.gu(15)
            height: units.gu(4)
            text: "Clear all results"
            onClicked:
            {
                PopupUtils.open(Qt.resolvedUrl("ClearSourcesDlg.qml"), mainView,
                {
                    title: "Clear all",
                    text: "This will clear all found faces result as well as where too search for faces"
                })
                
            }
        }
        
        Rectangle
        {
            id: searchSourcesListViewRectangle
            anchors.left: parent.left
            anchors.top: buttonPerformSearch.bottom            
            color: "white"
            border.width: 2
            border.color: "black"               
            radius: 2               
            width: units.gu(40); height: units.gu(45)        
            visible: false
        
            ListView 
            {
                id: searchSourcesListView
                width:  units.gu(40)
                height: units.gu(45)
                visible:false

                spacing: 2
                model : searchSourcesListModelObj
                clip: true
                focus: true
                delegate: Component 
                {
                    Item 
                    {
                        width: parent.width
                        height: units.gu(5)
                        Row 
                        {
                            Text { text:  model.textItemRole }
                        }                    
                    }
                }            
            }
        }        
        /////////////////////////////////////////////////////////
        
        /////////////////// Faces view ///////////////////
        GridView 
        {
            id:facesGridView
            visible: false
            anchors.left: parent.left
            anchors.top: buttonArea.bottom            

            width: units.gu(45)
            height: units.gu(65)
            cellWidth: units.gu(15)
            cellHeight: units.gu(15)
            clip:true

            model: faceListModelObj
            
            delegate: Column 
            {            
                spacing: units.gu(3)
                Item 
                {
                    width: units.gu(15)
                    height: units.gu(15)                
                    Image
                    {                   
                        source: faceListModelObj.getImgSource(index)
                        anchors.horizontalCenter: parent.horizontalCenter 
                        width: units.gu(15)
                        height: units.gu(15)                        
                    }
                    MouseArea
                    {
                        anchors.fill: parent
                        onClicked : 
                        {
                            faceListModelObj.setSelectedImgSource(index)
                            PopupUtils.open(Qt.resolvedUrl("SingleImageDlg.qml"), mainView,
                            {
                                title: faceListModelObj.getSelectedImgSource()
                            })                            
                            
                        }
                    }                    
                }
                
            }
        }
        /////////////////////////////////////////////////////////
        
        
        TextEdit
        {
            anchors.left: parent.left
            anchors.top: buttonArea.bottom
            id: introTextPart1
            width:  units.gu(43)
            height: units.gu(20)
            textFormat: TextEdit.RichText
            text: "<b>Choose Folders</b><p>Please select folders to start searching for faces. Browse to a folder you want to search for faces and press the Add button.</p>\
<b>Search for faces</b><p>\
<p>When you are done adding folders press the Search button. You can do Add and Search independent of eachother.</p>"
            readOnly: true
            wrapMode: TextEdit.WordWrap
        }        
       
        Item
        {
            id: indexPath
            anchors.left: parent.left
            anchors.top: introTextPart1.bottom         
            width: units.gu(37)
            height: units.gu(3)
            clip: true
            Rectangle
            {
                id: inputIndexPathRect
                color: "white"
                border.width: 2
                border.color: "black"               
                radius: 2               
                width: units.gu(37); height: units.gu(3)
                TextInput
                {
                    id: inputIndexPath               
                    color: "black"
                    text: "/home/phablet/Pictures"
                    readOnly: true
                    inputMethodHints: Qt.ImhNoPredictiveText
                                      
                    width: parent.width
                    height: parent.height
                    anchors.left: parent.left
                    anchors.top: parent.top
                    anchors.leftMargin : parent.border.width
                    anchors.rightMargin : parent.border.width
                    anchors.topMargin : parent.border.width
                    anchors.bottomMargin : parent.border.width
                   
                    focus: true                   
                }
            }
        }
        Button
        {
            id:buttonAddFolder
            anchors.left: indexPath.right
            anchors.top: indexPath.top
        
            width: units.gu(6)
            height: units.gu(3)
            text: "Add"
            onClicked:
            {
                faceFinderObj.startIndexingFolder(inputIndexPath.text)
                
                PopupUtils.open(Qt.resolvedUrl("InfoDlg.qml"), mainView,
                {
                    title: "Folder added",
                    text: inputIndexPath.text
                })
                
            }
        }                
        
        Rectangle
        {
            id: folderListRectangle
            anchors.left: indexPath.left
            anchors.top: indexPath.bottom            
            color: "white"
            border.width: 2
            border.color: "black"               
            radius: 2               
            width: units.gu(43); height: units.gu(40)        
            ListView 
            {
                id:folderList
                width:  units.gu(43)
                height: units.gu(40)

                spacing: 2
                model : directoryBrowserObj
                clip: true
                focus: true
                delegate: Component 
                {
                    id: theComponentLeft
                    Item 
                    {
                        id: listItemLeft
                        width: parent.width
                        height: units.gu(6)
                        Row 
                        {
                            Image
                            {
                                source: directoryBrowserObj.getImgSource(index)

                            }                                        
                            Text { text:  model.fileOrFolderRole }
                        }
                        MouseArea
                        {
                            anchors.fill: parent
                            onClicked : 
                            {                            
                                inputIndexPath.text = directoryBrowserObj.doDoubleClicked(index)                            
                            }
                        }                    
                    }
                }            
            }
        }
 


        Timer 
        {
            interval: 500;
            running: true;
            onTriggered:
            {
                PopupUtils.open(Qt.resolvedUrl("StartupDlg.qml"), mainView,
                {
                    title: "Welcome"
                })
            }
        }
        
        Timer 
        {
            interval: 30000; 
            running: true; 
            repeat: true
            onTriggered: refreshImagesInFaceViewGridAndPollProcessFiles()
        }               

        Timer 
        {
            interval: 2000; 
            running: true; 
            repeat: true
            onTriggered: updateStatusBar()
        }               
        TextEdit
        {        
            id: statusBar
            anchors.left: parent.left
            anchors.bottom: parent.bottom            
            width:  units.gu(43)
            height: units.gu(4)
            textFormat: TextEdit.RichText
            text: "<b>Images scanned: 0 / 0"
            readOnly: true
            wrapMode: TextEdit.WordWrap
        }      

    }
}