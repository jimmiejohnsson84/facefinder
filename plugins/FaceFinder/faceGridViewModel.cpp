#include <QDebug>
#include <QStandardPaths>
#include <fstream>

#include "filesOperations.h"
#include "faceGridViewModel.h"
#include "filesDefine.h"

FaceListModel::FaceListModel(QObject *parent) : QAbstractListModel(parent)
{
    qDebug() << "FaceListModel constructor";   
    
    m_appDataPath = QDir(QStandardPaths::writableLocation(QStandardPaths::AppDataLocation));
}
//-------------------------------------------------------------------------------------------------
int FaceListModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    return this->m_imagePath.size();
}
//-------------------------------------------------------------------------------------------------
QHash<int, QByteArray> FaceListModel::roleNames() const
{
    QHash<int, QByteArray> roles;
    roles[theTextRole] = "facePath";
    
    return roles;
}
//-------------------------------------------------------------------------------------------------
QVariant FaceListModel::data(const QModelIndex &index, int role) const
{
    if(m_imagePath.size() != 0)
    {
        if (role == theTextRole)
        {
            return m_imagePath[index.row()];
        }

        return QVariant();
    }
    return QVariant();

}
//-------------------------------------------------------------------------------------------------
QString FaceListModel::getImgSource(QVariant index)
{
    if(m_imagePath.size() >= index.toInt())
    {
        QVariant facePath = m_imagePath[index.toInt()];        
        return QString(m_imagePath[index.toInt()]);
    }
    return QString("");
}   
//-------------------------------------------------------------------------------------------------
void FaceListModel::setSelectedImgSource(QVariant index)
{
    if(m_imagePath.size() >= index.toInt())
    {
        QVariant facePath = m_imagePath[index.toInt()];        
        m_selectedImagePath = facePath.toString().toStdString();
    }    
}
//-------------------------------------------------------------------------------------------------
bool FaceListModel::appendRow(QString data)
{
    int lastElemPos = this->m_imagePath.size();
    beginInsertRows(QModelIndex(), lastElemPos, lastElemPos);
    m_imagePath << data;
    
    m_imagePathSet.insert(data.toStdString());    

    endInsertRows();

    return true;
}
//-------------------------------------------------------------------------------------------------
bool FaceListModel::myRemoveRows(int position, int rows)
{
    beginRemoveRows(QModelIndex(), position, position+rows-1);

    for (int row=0; row < rows; ++row) 
    {
        m_imagePath.removeAt(position);
    }

    endRemoveRows();
    return true;
}
//-------------------------------------------------------------------------------------------------
void  FaceListModel::updateModel()
{   
    std::string path = m_appDataPath.path().toStdString();
    if(path.back() != '/')
    {
        path.append("/");
    }
    path.append(FACE_FOUND_FILE);
    
    std::ifstream file(path.c_str(), std::ios::in);    
    if(file.good())
    {
        for (std::string line; std::getline(file, line); )
        {
            if(m_imagePathSet.find(line) ==  m_imagePathSet.end())
            {
                appendRow(QString(line.c_str()));
            }
        }
    }    
    file.close();    
}
//-------------------------------------------------------------------------------------------------
QString FaceListModel::getSelectedImgSource()
{
    return QString(m_selectedImagePath.c_str());
}

