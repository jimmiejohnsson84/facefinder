#include <QtQml>
#include <QtQml/QQmlContext>

#include "plugin.h"
#include "facefinder.h"
#include "directoryBrowser.h"
#include "searchSourcesListModel.h"
#include "faceGridViewModel.h"

void FaceFinderPlugin::registerTypes(const char *uri)
{
    //@uri FaceFinder
    qmlRegisterSingletonType<FaceFinder>(uri, 1, 0, "FaceFinder", [](QQmlEngine*, QJSEngine*) -> QObject* { return new FaceFinder; });
    qmlRegisterType<FaceFinderHelper>(uri, 1, 0, "FaceFinderHelper");
    qmlRegisterType<DirectoryBrowser>(uri, 1, 0, "DirectoryBrowser");
    qmlRegisterType<SearchSourcesListModel>(uri, 1, 0, "SearchSourcesListModel");
    qmlRegisterType<FaceListModel>(uri, 1, 0, "FaceListModel");    
}
