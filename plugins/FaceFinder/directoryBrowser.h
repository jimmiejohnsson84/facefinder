#ifndef DIRECTORYBROWSER_H
#define DIRECTORYBROWSER_H

#include <QObject>
#include <QVariant>
#include <QAbstractListModel>

// A simple model to show directories and allow user to navigate back/forth between directories
class DirectoryBrowser : public QAbstractListModel
{
    Q_OBJECT

public:    
    enum theRoles {theTextRole= Qt::UserRole + 1};
    
    DirectoryBrowser(QObject *parent = 0);
    int rowCount(const QModelIndex &parent) const;
    QVariant data(const QModelIndex &index, int role) const;
    bool appendRow(QString data);
    bool myRemoveRows(int position, int rows);
    
    Q_INVOKABLE void                updateFolder();
    Q_INVOKABLE QString             filesAndFoldersFromFolder(QVariant folderName);    

    Q_INVOKABLE QString             selectedDirectory(); // Current directory of the model
    
    Q_INVOKABLE QString             FolderToVisitOnDoubleClick(QVariant index);
    
    Q_INVOKABLE QString             doDoubleClicked(QVariant index);
    Q_INVOKABLE QString             getImgSource(QVariant index);
    Q_INVOKABLE void                setNavigateToFolder(QVariant path);
    Q_INVOKABLE void                navigateToFolderSet();
    
protected :    
    QHash<int, QByteArray> roleNames() const;
    
private:
      
    QList <QString> m_filesAndFoldersNames;
    QString m_selectedDir;
    
    QString m_navigateToFolder;
};



#endif
