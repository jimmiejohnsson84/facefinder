#ifndef FACELISTMODEL_H
#define FACELISTMODEL_H

#include <QObject>
#include <QVariant>
#include <QDir>
#include <QAbstractListModel>
#include <set>

// Model used by the gridview that shows the faces
class FaceListModel : public QAbstractListModel
{
    Q_OBJECT

public:    
    enum theRoles {theTextRole= Qt::UserRole + 1};
    
    FaceListModel(QObject *parent = 0);
    int rowCount(const QModelIndex &parent) const;
    QVariant data(const QModelIndex &index, int role) const;
    bool appendRow(QString data);
    bool myRemoveRows(int position, int rows);
    
    /*
        Reads back all faces from FACE_FOUND_FILE 
    */
    Q_INVOKABLE void                updateModel();
    
    Q_INVOKABLE QString             getImgSource(QVariant index);    
    Q_INVOKABLE void                setSelectedImgSource(QVariant index);
    
    Q_INVOKABLE QString             getSelectedImgSource();
    
protected :    
    QHash<int, QByteArray> roleNames() const;
    
private:
      
    QList <QString> m_imagePath;
    
    /*
        For some reason, removing items here wont work properly (myRemoveRows during updateModel seems to crash the app...). Therefore, I also load them to this 
        set - and if I find the image in this set on updateModel, I dont append it to the m_imagePath list.
    */
    std::set<std::string> m_imagePathSet;
    
    std::string m_selectedImagePath;
    
    QDir m_appDataPath;
};



#endif
