#include <QDebug>
#include <algorithm>

#include "filesOperations.h"
#include "directoryBrowser.h"

DirectoryBrowser::DirectoryBrowser(QObject *parent) : QAbstractListModel(parent)
{
    qDebug() << "DirectoryBrowser constructor";   
    m_selectedDir = "/home/phablet/Pictures";
    filesAndFoldersFromFolder(m_selectedDir);
}
//-------------------------------------------------------------------------------------------------
int DirectoryBrowser::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    return this->m_filesAndFoldersNames.size();
}
//-------------------------------------------------------------------------------------------------
QHash<int, QByteArray> DirectoryBrowser::roleNames() const
{
    QHash<int, QByteArray> roles;
    roles[theTextRole] = "fileOrFolderRole";
    
    return roles;
}
//-------------------------------------------------------------------------------------------------
QVariant DirectoryBrowser::data(const QModelIndex &index, int role) const
{
    if(m_filesAndFoldersNames.size() != 0)
    {
        if (role == theTextRole)
        {
            return m_filesAndFoldersNames[index.row()];
        }

        return QVariant();
    }
    return QVariant();

}
//-------------------------------------------------------------------------------------------------
QString DirectoryBrowser::getImgSource(QVariant index)
{
    if(m_filesAndFoldersNames.size() != 0)
    {
        std::string path = m_selectedDir.toStdString();
        QVariant fileOrFolder = m_filesAndFoldersNames[index.toInt()];
        
        if(path.back() != '/')
        {
            path += std::string("/");
        }
        path += fileOrFolder.toString().toStdString();
        
        if(filesOperations::isDirectory(path.c_str()))
        {
            return QString("../assets/folder.png");
        }
    }
    return QString("../assets/file.png");
}   
//-------------------------------------------------------------------------------------------------
void DirectoryBrowser::setNavigateToFolder(QVariant path)
{
    m_navigateToFolder = path.toString();
}
//-------------------------------------------------------------------------------------------------
void DirectoryBrowser::navigateToFolderSet()
{
    if(!m_navigateToFolder.isEmpty())
    {
        filesAndFoldersFromFolder(m_navigateToFolder);
        m_navigateToFolder = "";
    }
}
//-------------------------------------------------------------------------------------------------
QString DirectoryBrowser::FolderToVisitOnDoubleClick(QVariant index)
{
    if(m_filesAndFoldersNames.size() != 0)
    {
        QVariant itemDoubleClicked = m_filesAndFoldersNames[index.toInt()];
        // if it is a directory, traverse it        
        std::string folderToVisit = m_selectedDir.toStdString();
        std::string stdStringitemDoubleClicked = itemDoubleClicked.toString().toStdString();
        
        // .. means go up
        if(stdStringitemDoubleClicked.compare("..") == 0)
        {
            if(folderToVisit.find("/") != std::string::npos && folderToVisit.length() > 1)
            {
                if(folderToVisit.back() == '/')
                {
                    folderToVisit.pop_back();
                }
                
                if(folderToVisit.find_last_of("/") != std::string::npos)
                {
                    if(folderToVisit.find_last_of("/") == 0) // We are at the root
                    {
                        folderToVisit = "/";
                    }
                    else
                    {
                        folderToVisit = folderToVisit.substr(0, folderToVisit.find_last_of("/"));
                    }
                }
            }
        }
        else
        {
            #ifdef DEBUG
            qDebug() << "clicked on actual folder" ;
            #endif
            
            if(folderToVisit.back() != '/')
            {
                folderToVisit += std::string("/");
            }
            folderToVisit += itemDoubleClicked.toString().toStdString();
        }
        
        #ifdef DEBUG
        qDebug() << "doDoubleClicked: " << folderToVisit.c_str();
        #endif
        
        if(filesOperations::isDirectory(folderToVisit.c_str()))
        {
            return QString(folderToVisit.c_str());
        }        
    }    
    return m_selectedDir; // Stay in same folder
}
//-------------------------------------------------------------------------------------------------
QString DirectoryBrowser::doDoubleClicked(QVariant index)
{   
    return filesAndFoldersFromFolder(FolderToVisitOnDoubleClick(index));
}
//-------------------------------------------------------------------------------------------------
bool DirectoryBrowser::appendRow(QString data)
{
    int lastElemPos = this->m_filesAndFoldersNames.size();
    beginInsertRows(QModelIndex(), lastElemPos, lastElemPos);
    m_filesAndFoldersNames << data;  

    endInsertRows();

    return true;
}
//-------------------------------------------------------------------------------------------------
bool DirectoryBrowser::myRemoveRows(int position, int rows)
{
    beginRemoveRows(QModelIndex(), position, position+rows-1);

    for (int row=0; row < rows; ++row) 
    {
        m_filesAndFoldersNames.removeAt(position);
    }

    endRemoveRows();
    return true;
}
//-------------------------------------------------------------------------------------------------
void  DirectoryBrowser::updateFolder()
{
    filesAndFoldersFromFolder(m_selectedDir);
}
//-------------------------------------------------------------------------------------------------
QString DirectoryBrowser::filesAndFoldersFromFolder(QVariant folderName)
{
    qDebug() << "DirectoryBrowser filesAndFoldersFromFolder" << folderName;
    
    std::vector<std::string> filesAndFolders;   
    int status = filesOperations::getDirFiles(folderName.toString().toStdString().c_str(), filesAndFolders, true);    
    if(status == 0)
    {
        int lastElemPos = m_filesAndFoldersNames.size();
        myRemoveRows(0, lastElemPos);
        
        m_selectedDir = folderName.toString();
        
        for(std::vector<std::string>::iterator it = filesAndFolders.begin(); it != filesAndFolders.end(); it++)
        {
            if((*it).compare(".") != 0)
            {
                // We only want to display folders
                std::string currentPath = folderName.toString().toStdString();
                if(currentPath.back() != '/')
                {
                    currentPath += std::string("/");
                }
                currentPath  += (*it); 
                qDebug() << "currentPath " << currentPath.c_str();
                if(filesOperations::isDirectory(currentPath.c_str()))
                {
                    appendRow(QString((*it).c_str()));
                }
            }
        }
    }
    
    return m_selectedDir;
}
//-------------------------------------------------------------------------------------------------
QString DirectoryBrowser::selectedDirectory()
{
    return m_selectedDir;
}

