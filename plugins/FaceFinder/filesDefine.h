#ifndef FILEDEFINES_H
#define FILEDEFINES_H

#define FOLDERS_TO_INDEX "FaceFinder_folderstosync.txt"
#define FACE_FOUND_FILE "FaceFinder_facesfound.txt"
#define FACE_NOT_FOUND_FILE "FaceFinder_facesnotfound.txt"

#endif
