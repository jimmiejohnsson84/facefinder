#include <sys/types.h>
#include <dirent.h>
#include <errno.h>
#include <cstdio>
#include <sys/stat.h>
#include <time.h>
#include <string.h>
#include <unistd.h>
#include <ftw.h>
#include <iostream>
#include <algorithm>
#include <sstream> 
#include <fstream>
#include "filesOperations.h"

using namespace std;

#define DEBUG

//----------------------------------------------------------------------------------------
bool filesOperations::writeToFile(const char *path, const char* data, bool trunc)
{
    std::ofstream ofs;
    
    std::ios_base::openmode openMode = std::ofstream::out | std::ofstream::app;
    if(trunc)
    {
        openMode = std::ofstream::out | std::ofstream::trunc;
    }
    
    ofs.open (path, openMode);
    
    if(ofs.good())
    {
        ofs << data;
    }
    else        
    {
        return false;
    }
    ofs.close();
    
    return ofs.good();
}
//----------------------------------------------------------------------------------------
bool sortString(string a,string b) 
{    
    return (a < b);
}
//----------------------------------------------------------------------------------------
int filesOperations::isDirectory(const char *path) 
{
   struct stat statbuf;
   if (stat(path, &statbuf) != 0)
       return 0;
   return S_ISDIR(statbuf.st_mode);
}
//----------------------------------------------------------------------------------------
int filesOperations::isFile(const char *path) 
{
   struct stat statbuf;
   if (stat(path, &statbuf) != 0)
   {
       return 0;
   }
   return S_ISREG(statbuf.st_mode);
}            
//----------------------------------------------------------------------------------------
int filesOperations::getDirFiles(const char* dir, vector<string> &filesAndFolders, bool sort)        
{
    DIR *dp;
    struct dirent *dirp;                
    errno = 0;
    dp  = opendir(dir);
    if(!dp) 
    {
        return errno;
    }

    while ((dirp = readdir(dp)) != NULL) 
    {                
        filesAndFolders.push_back(string(dirp->d_name));
    }                      
    closedir(dp);

    if(sort)
    {
        std::sort (filesAndFolders.begin(), filesAndFolders.end(), sortString);
    }

    return 0;
}
//----------------------------------------------------------------------------------------
void filesOperations::getFileContents(const char* srcFilePath, std::string &fileContent)
{
    // First, make sure we are dealing with a regular file    
    if(!filesOperations::isFile(srcFilePath))
    {
        return;
    }

    const int BUFSIZE = 1024;
    char buf[BUFSIZE];
    memset(buf, '\0', BUFSIZE);
    size_t size;
    
    FILE* srcFile = fopen(srcFilePath, "rb");
    
    if(srcFile)
    {
        while(size = fread(buf, 1, BUFSIZE, srcFile))
        {
            fileContent.append(buf, size);
            memset(buf, '\0', BUFSIZE);
        }
    }        
}