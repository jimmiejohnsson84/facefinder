#include <vector>
#include <string>

class filesOperations
{
    public:    
    static int isDirectory(const char *path);
    static int isFile(const char *path);

    static void getFileContents(const char* srcFilePath, std::string &fileContent);
    static int getDirFiles(const char* dir, std::vector<std::string> &filesAndFolders, bool sort);
        
    
    static bool writeToFile(const char *path, const char* data, bool trunc = false);
    
};
