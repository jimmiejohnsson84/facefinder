#ifndef SEARCHSOURCESLISTMODEL_H
#define SEARCHSOURCESLISTMODEL_H

#include <QObject>
#include <QDir>
#include <QVariant>
#include <QAbstractListModel>


class SearchSourcesListModel : public QAbstractListModel
{
Q_OBJECT

public:    
    enum theRoles {textRole= Qt::UserRole + 1};
    
    SearchSourcesListModel(QObject *parent = 0);
    int rowCount(const QModelIndex &parent) const;
    QVariant data(const QModelIndex &index, int role) const;
    void appendRow(QString textData);
    void myRemoveRows(int position, int rows);
    
    Q_INVOKABLE void        populateFromFoldersToIndex();
    
protected :    
    QHash<int, QByteArray> roleNames() const;
    
private:   

    QDir m_appDataPath;
    QList <QString> m_searchFolderSourcePaths;
};


#endif
