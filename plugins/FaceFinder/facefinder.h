#ifndef FACEFINDER_H
#define FACEFINDER_H

#include <map>
#include <set>
#include <vector>
#include <future>
#include <string>


#include <QObject>
#include <QDir>
#include <QVariant>


class FaceFinder: public QObject 
{
    Q_OBJECT

public:
    FaceFinder();
    ~FaceFinder() = default;

    Q_INVOKABLE void speak();
};

class FaceFinderHelper: public QObject 
{
    Q_OBJECT

public:
    FaceFinderHelper();
    ~FaceFinderHelper();

    Q_INVOKABLE void        startIndexingFolder(QVariant folderName); // Add files in this directory and its sub directories to process
    
    // Removes all the folders to search for pictures from
    Q_INVOKABLE void        clearFoldersToIndex();
    
    // Process files put in from startIndexingFolder
    Q_INVOKABLE void        processFiles();    
    
    // Called from timer in Main qml file to start new async jobs if there is still work left to be done
    Q_INVOKABLE void        pollProcessFiles();
    
    // The amount of files that have been scanned
    Q_INVOKABLE QString    nrFilesScanned();
    
    // Total amount of files (scanned and unscanned)
    Q_INVOKABLE QString    totalNrFiles();
    

    // Returns a path to the apps data storage location + fileName
    static std::string         appDataPath(const char* fileName);


private:
    
    /*
        Everytime we run processFiles, we sync against the FACE_FOUND_FILE to make sure we do not try to process the same file twice,
        since it is expensive to do the face matching
    */    
    void                syncFilesToIndexAgainstFaceFile();
    void                readIndexInfoFile(const char* path);
    
    /*
        sync folders that user has previously wanted to serach for faces in
    */
    void                syncFoldersToIndex();
        
    std::set<std::string> m_foldersToIndex;
    
        
    /* Files to index. first part is absolute path, second part is bool indicating if the file has been tested or not. The idea is
        to fill this map with files to consider when user press "Search" for a path. Then when user presses index, we process this list and update files which we have already considered
    */
    std::map<std::string, bool> m_filesToIndex;
    
    /* Set of the files which have been processed and found to have a face in them
    */
    //std::set<std::string> m_filesWithFaces;
    
    std::vector<std::future<int>> m_futures;
    
    bool        m_runProcessFiles;
    
    static QDir m_appDataPath;
};

#endif
