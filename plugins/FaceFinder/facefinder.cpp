#include <QDebug>
#include <QStandardPaths>
#include <algorithm>
#include <ctime>
#include <mutex>

#include "dlib/image_processing/frontal_face_detector.h"
#include "dlib/gui_widgets.h"
#include "dlib/image_io.h"

#include "filesOperations.h"
#include "facefinder.h"
#include "filesDefine.h"



// Make sure the writing threads don't try to open the file for writing simultaneously and also that we dont read back from a face index file while it is being written to
std::mutex g_faceFileMutex;

// Avoid re-entrance into processFiles
std::mutex g_processFiles;

unsigned int g_nrThreadsActive = 0;

FaceFinder::FaceFinder() 
{   
}
//----------------------------------------------------------------------------------------
void FaceFinder::speak() 
{   
    qDebug() << "FaceFinder started";
}
//----------------------------------------------------------------------------------------


//----------------------------------------------------------------------------------------
QDir FaceFinderHelper::m_appDataPath;

FaceFinderHelper::FaceFinderHelper()
{
    qDebug() << "FaceFinderHelper startIndexingFolder";
    
    m_runProcessFiles = false;
    m_appDataPath = QDir(QStandardPaths::writableLocation(QStandardPaths::AppDataLocation));
    
    if(!m_appDataPath.exists())
    {
        m_appDataPath.mkpath(QStandardPaths::writableLocation(QStandardPaths::AppDataLocation));
    }    
    
    qDebug() << "appDataPath set to: " << m_appDataPath;
    
    syncFoldersToIndex();
}
//----------------------------------------------------------------------------------------
FaceFinderHelper::~FaceFinderHelper()
{
}
//----------------------------------------------------------------------------------------
void FaceFinderHelper::syncFoldersToIndex()
{
    std::string path = appDataPath(FOLDERS_TO_INDEX);
    
    std::ifstream file(path.c_str(), std::ios::in);    
    if(file.good())
    {
        for (std::string line; std::getline(file, line); )
        {
            m_foldersToIndex.insert(line);
            qDebug() << "folder to sync: " << line.c_str();
        }
    }    
    file.close();
    
    for (std::set<std::string>::iterator it = m_foldersToIndex.begin(); it != m_foldersToIndex.end(); it++)
    {
        startIndexingFolder(QVariant((*it).c_str()));
    }
}
//----------------------------------------------------------------------------------------
std::string FaceFinderHelper::appDataPath(const char* fileName)
{
    std::string path = m_appDataPath.path().toStdString();
    if(path.back() != '/')
    {
        path.append("/");
    }
    path.append(fileName);
    
    return path;
}
//----------------------------------------------------------------------------------------
void FaceFinderHelper::startIndexingFolder(QVariant folderName)
{    
    qDebug() << "FaceFinderHelper startIndexingFolder" << folderName;
    
    std::string folderStdString = folderName.toString().toStdString();    
    if(folderStdString.back() != '/')
    {
        folderStdString += std::string("/");
    }
    
    std::vector<std::string> filesAndFolders;
    int status = filesOperations::getDirFiles(folderStdString.c_str(), filesAndFolders, false);    
    if(status == 0)
    {        
        for(std::vector<std::string>::iterator it = filesAndFolders.begin(); it != filesAndFolders.end(); it++)
        {
            if((*it).compare(".") != 0)
            {
                qDebug() << "File/dir: " << (*it).c_str();
                
                if( (*it).find(".jpg") != std::string::npos || (*it).find(".jpeg") != std::string::npos)
                {
                    std::string pathToTest = folderStdString + (*it);
                                        
                    if(filesOperations::isFile(pathToTest.c_str()))
                    {
                        qDebug() << "Adding " << pathToTest.c_str() << " to set of files to index";
                        m_filesToIndex.insert(std::pair<std::string, bool>(pathToTest, false));                        
                    }
                    else
                    {
                        qDebug() << "Couldnt load the picture, skipping " << pathToTest.c_str();
                    }
                }                    
            }
        }
    }
    
    qDebug() << "Updating folders to index file" << folderStdString.c_str(); 
    m_foldersToIndex.insert(folderStdString);

    std::string foldersToIndexContent;
    for (std::set<std::string>::iterator it = m_foldersToIndex.begin(); it != m_foldersToIndex.end(); it++)
    {
        foldersToIndexContent += (*it) + "\n";
    }
    
    std::string pathFoldersToIndex = appDataPath(FOLDERS_TO_INDEX);
    filesOperations::writeToFile(pathFoldersToIndex.c_str(), foldersToIndexContent.c_str(), true);
}
//----------------------------------------------------------------------------------------
void FaceFinderHelper::clearFoldersToIndex()
{    
    std::string pathFoldersToIndex = appDataPath(FOLDERS_TO_INDEX);
    std::string pathFaceFound = appDataPath(FACE_FOUND_FILE);
    std::string pathFaceNotFound = appDataPath(FACE_NOT_FOUND_FILE);
    
    
    std::lock_guard<std::mutex> guard(g_faceFileMutex);                
    filesOperations::writeToFile(pathFoldersToIndex.c_str(), "", true);
    filesOperations::writeToFile(pathFaceFound.c_str(), "", true);
    filesOperations::writeToFile(pathFaceNotFound.c_str(), "", true);
    
    m_foldersToIndex.clear();
    m_filesToIndex.clear();
}
//----------------------------------------------------------------------------------------
int processFile(const char *path)
{
    dlib::frontal_face_detector detector = dlib::get_frontal_face_detector();
    dlib::array2d<unsigned char> img;

    qDebug() << "Processing " << path ;

    dlib::load_image(img, path);
    //dlib::pyramid_up(img);// Scales picture up x2, better accuracy but also takes longer time...
    std::vector<dlib::rectangle> dets = detector(img);

    if(dets.size() > 0)
    {
        std::lock_guard<std::mutex> guard(g_faceFileMutex);
        std::string faceFilePath = path;
        faceFilePath += "\n";
        
        std::string pathFaceFound = FaceFinderHelper::appDataPath(FACE_FOUND_FILE);
        filesOperations::writeToFile(pathFaceFound.c_str(), faceFilePath.c_str());
        qDebug() << "found a face in the picture!";
        g_nrThreadsActive--;
        
        return 1;
    }
    else
    {
        std::lock_guard<std::mutex> guard(g_faceFileMutex);        
        std::string noFaceFilePath = path;
        noFaceFilePath += "\n";
        
        std::string pathFaceNotFound = FaceFinderHelper::appDataPath(FACE_NOT_FOUND_FILE);
        filesOperations::writeToFile(pathFaceNotFound.c_str(), noFaceFilePath.c_str());                
        qDebug() << "No face found in the picture";
    }
    
    g_nrThreadsActive--;
    return 0;
}
//------------------------------------------
void FaceFinderHelper::processFiles()
{
    // Avoid re-entrance from timer
    std::lock_guard<std::mutex> guard(g_processFiles);
    
    qDebug() << "FaceFinderHelper::processFiles " ;
    
    syncFilesToIndexAgainstFaceFile();    
    for(std::map<std::string, bool>::iterator it = m_filesToIndex.begin(); it != m_filesToIndex.end(); it++) 
    {
        if(g_nrThreadsActive < 20)
        {
            if(!(*it).second)
            {
                (*it).second = true;

                g_nrThreadsActive++;
                m_futures.push_back(std::async(std::launch::async, processFile, (*it).first.c_str()));                
            }
        }
        else
        {
            m_runProcessFiles = true;
            break;
        }        
    }
}    
//----------------------------------------------------------------------------------------
void FaceFinderHelper::pollProcessFiles()
{
    if(m_runProcessFiles)
    {
        m_runProcessFiles = false;
        processFiles();
    }
        
}
//----------------------------------------------------------------------------------------
QString FaceFinderHelper::nrFilesScanned()
{
    // We need to count from the files - we dont know if the user has started a search in previous session and is coming back    
    int nrFilesScanned = 0;

    std::string pathFaceFound = appDataPath(FACE_FOUND_FILE);
    std::lock_guard<std::mutex> guard(g_faceFileMutex);
    std::ifstream fileFacesFound(pathFaceFound.c_str(), std::ios::in);    
    if(fileFacesFound.good())
    {
        for (std::string line; std::getline(fileFacesFound, line); )
        {
            nrFilesScanned++;
        }
    }    
    fileFacesFound.close();

    std::string pathFaceNotFound = appDataPath(FACE_NOT_FOUND_FILE);
    std::ifstream fileFacesNotFound(pathFaceNotFound.c_str(), std::ios::in);    
    if(fileFacesNotFound.good())
    {
        for (std::string line; std::getline(fileFacesNotFound, line); )
        {
            nrFilesScanned++;
        }
    }    
    fileFacesNotFound.close();
    return QString::number(nrFilesScanned);
}
//----------------------------------------------------------------------------------------
QString FaceFinderHelper::totalNrFiles()
{
    return QString::number(m_filesToIndex.size());
}
//----------------------------------------------------------------------------------------    
void FaceFinderHelper::readIndexInfoFile(const char* path)
{
    // Most likely, we dont need to lock the file for reading - as long as you just read the file, it wont matter if someone else is trying to write to it.
    // But rather safe than sorry. Also, parsing of the file might break if someone writes to the file while its being read back.       
    std::lock_guard<std::mutex> guard(g_faceFileMutex);
    
    std::ifstream file(path, std::ios::in);    
    if(file.good())
    {
        for (std::string line; std::getline(file, line); )
        {
            std::map<std::string, bool>::iterator it = m_filesToIndex.find(line);
            
            if(it != m_filesToIndex.end())
            {
                m_filesToIndex.at(line) = true;
                qDebug() << "face synced: " << line.c_str();
            }
            else
            {
                m_filesToIndex.insert(std::pair<std::string, bool>(line, true));
                qDebug() << "face synced(added): " << line.c_str();
            }
        }
    }    
    file.close();

}
//----------------------------------------------------------------------------------------
void FaceFinderHelper::syncFilesToIndexAgainstFaceFile()
{
    qDebug() << "FaceFinderHelper::syncFilesToIndexAgainstFaceFile";
       
    // Read back the faces found.    
    std::string pathFaceFound = appDataPath(FACE_FOUND_FILE);
    readIndexInfoFile(pathFaceFound.c_str());

    // Do the same for the not found faces
    std::string pathFaceNotFound = appDataPath(FACE_NOT_FOUND_FILE);
    readIndexInfoFile(pathFaceNotFound.c_str());
}