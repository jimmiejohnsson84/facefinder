#include <QDebug>
#include <QStandardPaths>
#include <fstream>
#include <string>

#include "searchSourcesListModel.h"
#include "filesDefine.h"

SearchSourcesListModel::SearchSourcesListModel(QObject *parent) : QAbstractListModel(parent)
{
    m_appDataPath = QDir(QStandardPaths::writableLocation(QStandardPaths::AppDataLocation));
}
//-------------------------------------------------------------------------------------------------
int SearchSourcesListModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    return m_searchFolderSourcePaths.size();
}
//-------------------------------------------------------------------------------------------------
QHash<int, QByteArray> SearchSourcesListModel::roleNames() const
{
    QHash<int, QByteArray> roles;
    roles[textRole] = "textItemRole";
    
    return roles;
}
//-------------------------------------------------------------------------------------------------
QVariant SearchSourcesListModel::data(const QModelIndex &index, int role) const
{
    if(m_searchFolderSourcePaths.size() != 0)
    {
        if (role == textRole)
        {
            return m_searchFolderSourcePaths[index.row()];
        }

        return QVariant();
    }
    return QVariant();

}
//-------------------------------------------------------------------------------------------------
void SearchSourcesListModel::appendRow(QString textData)
{
    int lastElemPos = m_searchFolderSourcePaths.size();
    beginInsertRows(QModelIndex(), lastElemPos, lastElemPos);
    m_searchFolderSourcePaths << textData;
    
    endInsertRows();
}
//-------------------------------------------------------------------------------------------------
void SearchSourcesListModel::myRemoveRows(int position, int rows)
{
    beginRemoveRows(QModelIndex(), position, position+rows-1);
    for (int row=0; row < rows; ++row) 
    {
        m_searchFolderSourcePaths.removeAt(position);
    }

    endRemoveRows();    
}
//-------------------------------------------------------------------------------------------------
void SearchSourcesListModel::populateFromFoldersToIndex()
{
    while(m_searchFolderSourcePaths.size() > 0)
    {
        int lastElemPos = m_searchFolderSourcePaths.size() - 1;    
        myRemoveRows(lastElemPos, 1);
    }    
        
    std::string path = m_appDataPath.path().toStdString();
    if(path.back() != '/')
    {
        path.append("/");
    }
    path.append(FOLDERS_TO_INDEX);
        
    std::ifstream file(path.c_str(), std::ios::in);    
    if(file.good())
    {
        for (std::string line; std::getline(file, line); )
        {
            appendRow(QString(line.c_str()));           
        }
    }    
    file.close();    
}